const fs = require('fs');

function deleteFiles(){
    fs.readFile('filename.txt', function(err, data){
        if(err){
            console.log(err)
        }
        let arr = data.toString().split(',')
        for(i=0; i<arr.length; i++){
           
            fs.unlink(arr[i], function(err){
                if(err){
                    return console.log(err)
                }
            })
        }
    })
}




function readFile() {
    
    fs.readFile('../data/fs-callbacks/lipsum.txt', function (err, data){
        if (err){
            return console.log(err);
        }
        fs.writeFile('node.txt',data.toString().toUpperCase(), function(err){
            fs.writeFileSync('../filename.txt', 'node.txt')
            if(err){
                return console.log(err)
            }
            fs.readFile('node.txt',function(err, data){
                if(err){
                    return console.log(err)
                }
                
                data = data.toString().replace(/([.?!])\s*(?=[A-Z])/g, "\n")
            
                fs.writeFile('node1.txt',data.toString().toLocaleLowerCase(), function(err){
                    fs.appendFileSync('../filename.txt', ',node1.txt')
                    if(err){
                        return console.log(err)
                    }
                    fs.readFile('node1.txt',function(err,data){
                        if(err){
                            return console.log(err)
                        }
                        let x = data.toString().split(' ').sort(function (first, second){
                            if(first.toLocaleUpperCase() < second.toLocaleUpperCase()){
                               return -1
                            } else if(first.toLocaleUpperCase() > second.toLocaleUpperCase()){
                               return 1
                            } else{
                               return 0
                            }
                         })
                        
                        sortString = ''
                        for(i=0; i<x.length; i++){
                            sortString += x[i] + ' '
                        }
                        
                        fs.writeFile('node2.txt', sortString, function(err, data){
                            fs.appendFileSync('../filename.txt', ',node2.txt')
                            if (err){
                                return console.log(err)
                            } 
                            deleteFiles()
                        })
                    })
                })
               
            })
        })
       
    })
}


//readFile();
//console.log(deleteFiles())


module.exports=readFile;