const lists = require('../data/trello-callbacks/lists.json');
const board = require('../data/trello-callbacks/boards.json');

function Infos(boardID,cb){ 

    if(typeof (boardID) !== 'string'){ //checks for correct input
        return cb("Pass Argument type to be string")
    }else{
    setTimeout(()=>{
        let boards = board.find((i)=> i['id'] == boardID)  // finds the boardID 
        
        if(boards !== undefined) {
            let obj = lists[boards.id]
            cb(obj)      // calling callback with result
        }else{
            cb('Wrong ID')
        }
        
    }, 2*1000)
}
}


module.exports=Infos;