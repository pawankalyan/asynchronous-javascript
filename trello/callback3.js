const cards = require('../data/trello-callbacks/cards.json');
const lists = require('../data/trello-callbacks/lists.json');

function cardsList(listID, cb){

    if(typeof (listID) !== 'string'){   // checks correct type of input is passed
        return cb("Pass Argument type to be string")
    }else{
    setTimeout(()=>{
        if (cards[listID] !== undefined){   // checks for correct ID
        let obj = cards[listID]
        cb(obj)   //calling callback with result
        }else{
            cb('Wrong ID')
        }
        
    },2*1000)
}
}

//cardsList("qwsa221", (obj)=>{console.log(obj)});
module.exports=cardsList;