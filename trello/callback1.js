const boards = require('../data/trello-callbacks/boards.json');

function boardsInfo (boardID, cb){
    if(typeof (boardID) !== 'string'){

        return cb("Pass Argument type to be string")

    }else{

	setTimeout(() => {

        let obj
        obj = boards.find((index)=>boardID == index.id)   //finds the ID in boards

        if(obj !== undefined) {

            cb(obj)    //call the callback function with result

        }else{
            
            cb('Wrong ID')

        }
	}, 2 * 1000);
}
    
}

//boardsInfo("mcu453ed")
module.exports=boardsInfo;